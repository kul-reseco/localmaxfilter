#################################
About the Tree Density Calculator
#################################

The *Tree Density Calculator* is a QGIS plugin and command line interface package designed to calculate tree densities
based on brightness images, using the local maximum of a sliding window.

The original Tree Density Calculator was written in C++ and ported to PyQGIS in 2018/2019. It has been developed in an
open source environment to encourage further development of the tool.

The **instruction pages** can be found at <https://treedensitycalculator.readthedocs.io>.

The code **repository** can be found at https://bitbucket.org/kul-reseco/localmaxfilter.

**PLEASE GIVE US CREDIT**

When using the Tree Density Calculator, please use the following citation:

*Crabbé, A. H., Cahy, T., Somers, B., Verbeke, L.P., Van Coillie, F. (2020). Tree Density Calculator Software*
*(Version x.x) [Software]. Available from https://bitbucket.org/kul-reseco/localmaxfilter.*

**ACKNOWLEDGEMENTS**

The software and user guide are based on the Local Maximum Filter [C++ software]:
Ghent University, Laboratory of Forest Management and Spatial Information Techniques, Lieven P.C. Verbeke

The revision into a QGIS plugin is funded primarily through BELSPO (the Belgian Science Policy Office) in the framework
of the STEREO III Programme – Project LUMOS - SR/01/321.

The LUMOS logo was created for free at https://logomakr.com.

**SOFTWARE LICENSE**

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.

