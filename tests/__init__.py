# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import sys
import numpy as np
import unittest
from qgis.core import QgsApplication
from qgis.analysis import QgsNativeAlgorithms

# Include code to work with processing algorithms
# ===============================================
if not os.environ.get('QGIS_PREFIX_PATH'):
    if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
        os.environ['QGIS_PREFIX_PATH'] = '/usr'
    else:
        os.environ['QGIS_PREFIX_PATH'] = os.path.join("C:", os.sep, "OSGeo4W64", "apps", "qgis")

QgsApplication.setPrefixPath(os.environ['QGIS_PREFIX_PATH'], True)
qgs = QgsApplication([], False)
qgs.initQgis()

# Append the path where processing plugin can be found
# Ubuntu Linux for example the prefix path is: /usr  and plugins: (qgis_prefix)/share/qgis/python/plugins
sys.path.append(os.path.join(os.environ['QGIS_PREFIX_PATH'], 'share', 'qgis', 'python', 'plugins'))  # unix, mac
# On Windows for example the prefix path is: C:\\OSGeo4W\\apps\\qgis\\ and plugins: (qgis_prefix)\\python\\plugins
sys.path.append(os.path.join(os.environ['QGIS_PREFIX_PATH'], 'python', 'plugins'))  # windows

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())


class ExtendedUnitTesting(unittest.TestCase):

    def assertEqualFloatArray(self, array1, array2, decimals):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def assertEqualIntArray(self, array1, array2):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertEqual(array1_line[i], array2_line[i])

    def assertEqualStringArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertEqual(array1[i], array2[i])
