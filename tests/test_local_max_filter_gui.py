# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : April 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Tree Density Calculator plugin and treedensitycalculator python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os import remove
from os.path import join, dirname, realpath, exists, basename
from osgeo import ogr
from qgis.core import QgsRasterLayer, QgsVectorLayer, QgsProject
from localmaxfilter.interfaces.local_max_filter_gui import LocalMaxFilterWidget
from tests import ExtendedUnitTesting

data_folder = join(dirname(realpath(__file__)), "testdata")


class TestLMFCLI(ExtendedUnitTesting):

    image = join(data_folder, 'polyg_15_100cm.tif')
    mask = join(data_folder, 'polyg_15_mask.shp')
    output = join(data_folder, 'gui_test_output')
    no_output_no_image = join(data_folder, 'gui_test_no_image')
    no_output_no_output = join(data_folder, 'gui_test_no_output')
    output_without_voronoi = join(data_folder, 'gui_test_no_voronoi')
    output_files = [image]

    @classmethod
    def setUpClass(cls):
        #  test_cannot_run_without_image_layer
        QgsProject.instance().removeAllMapLayers()

        cls.mask1 = '{0}_window_3_mask'.format(cls.no_output_no_image)
        cls.point1 = "{0}_window_3_point".format(cls.no_output_no_image)
        cls.voronoi1 = "{0}_window_3_voronoi".format(cls.no_output_no_image)
        cls.output_files.append(cls.mask1)
        cls.output_files.append(cls.point1)
        cls.output_files.append(cls.voronoi1)

        widget = LocalMaxFilterWidget()
        widget.outputFileWidget.lineEdit().setText(cls.no_output_no_image)
        widget._run_local_max_filter()
        widget.close()

        # test_cannot_run_without_output_layer
        QgsProject.instance().removeAllMapLayers()
        image_layer = QgsRasterLayer(cls.image, basename(cls.image), 'gdal')
        mask_layer = QgsVectorLayer(cls.mask, basename(cls.mask), 'ogr')
        QgsProject.instance().addMapLayers([image_layer, mask_layer])

        cls.mask2 = '{0}_window_3_mask'.format(cls.no_output_no_output)
        cls.point2 = "{0}_window_3_point".format(cls.no_output_no_output)
        cls.voronoi2 = "{0}_window_3_voronoi".format(cls.no_output_no_output)
        cls.output_files.append(cls.mask2)
        cls.output_files.append(cls.point2)
        cls.output_files.append(cls.voronoi2)

        widget = LocalMaxFilterWidget()
        widget._run_local_max_filter()
        widget.close()

        # test_voronoi_not_checked
        QgsProject.instance().removeAllMapLayers()
        image_layer = QgsRasterLayer(cls.image, basename(cls.image), 'gdal')
        mask_layer = QgsVectorLayer(cls.mask, basename(cls.mask), 'ogr')
        QgsProject.instance().addMapLayers([image_layer, mask_layer])

        cls.mask3 = '{0}_window_3_mask'.format(cls.output_without_voronoi)
        cls.point3 = "{0}_window_3_point".format(cls.output_without_voronoi)
        cls.voronoi3 = "{0}_window_3_voronoi".format(cls.output_without_voronoi)
        cls.output_files.append(cls.mask3)
        cls.output_files.append(cls.point3)
        cls.output_files.append(cls.voronoi3)

        widget = LocalMaxFilterWidget()
        widget.windowSpinBox.setValue(3)
        widget.outputFileWidget.lineEdit().setText(cls.output_without_voronoi)
        widget._run_local_max_filter()
        widget.close()

        # test_app_runs
        QgsProject.instance().removeAllMapLayers()
        image_layer = QgsRasterLayer(cls.image, basename(cls.image), 'gdal')
        mask_layer = QgsVectorLayer(cls.mask, basename(cls.mask), 'ogr')
        QgsProject.instance().addMapLayers([image_layer, mask_layer])

        cls.mask4 = '{0}_window_3_mask'.format(cls.output)
        cls.point4 = "{0}_window_3_point".format(cls.output)
        cls.voronoi4 = "{0}_window_3_voronoi".format(cls.output)
        cls.output_files.append(cls.mask4)
        cls.output_files.append(cls.point4)
        cls.output_files.append(cls.voronoi4)

        widget = LocalMaxFilterWidget()
        widget.windowSpinBox.setValue(3)
        widget.VoronoiCheckBox.setChecked(True)
        widget.outputFileWidget.lineEdit().setText(cls.output)
        widget._run_local_max_filter()
        widget.close()

    @classmethod
    def tearDownClass(cls) -> None:
        for file in cls.output_files:
            all_extensions = ['{}.shp'.format(file), '{}.dbf'.format(file), '{}.prj'.format(file),
                              '{}.shx'.format(file), '{}.qpj'.format(file), '{}.aux.xml'.format(file)]
            for ext in all_extensions:
                if exists(ext):
                    try:
                        remove(ext)
                    except PermissionError:
                        pass

    def test_app_opens(self):
        widget = LocalMaxFilterWidget()
        widget.show()
        widget.close()

    def test_app_runs(self):
        # mask
        data = ogr.Open('{}.shp'.format(self.mask4))
        layer = data.GetLayer()
        tree_count = []
        for feature in layer:
            tree_count.append(int(feature.GetField("TreeCount")))
        self.assertEqual(81, tree_count[0])
        self.assertEqual(84, tree_count[1])
        data = None

        # point
        data = ogr.Open('{}.shp'.format(self.point4))
        layer = data.GetLayer()
        number_trees = layer.GetFeatureCount()
        self.assertEqual(165, number_trees)
        data = None

        # voronoi
        data = ogr.Open('{}.shp'.format(self.voronoi4))
        layer = data.GetLayer()
        number_polygons = layer.GetFeatureCount()
        self.assertEqual(165, number_polygons)
        data = None

    def test_cannot_run_without_image_layer(self):
        # no output
        self.assertFalse(exists('{}.shp'.format(self.mask1)))
        self.assertFalse(exists('{}.shp'.format(self.point1)))
        self.assertFalse(exists('{}.shp'.format(self.voronoi1)))

    def test_cannot_run_without_output_layer(self):
        # no output
        self.assertFalse(exists('{}.shp'.format(self.mask2)))
        self.assertFalse(exists('{}.shp'.format(self.point2)))
        self.assertFalse(exists('{}.shp'.format(self.voronoi2)))

    def test_voronoi_not_checked(self):
        # mask
        data = ogr.Open('{}.shp'.format(self.mask3))
        layer = data.GetLayer()
        tree_count = []
        for feature in layer:
            tree_count.append(int(feature.GetField("TreeCount")))
        self.assertEqual(81, tree_count[0])
        self.assertEqual(84, tree_count[1])
        data = None

        # point
        data = ogr.Open('{}.shp'.format(self.point3))
        layer = data.GetLayer()
        number_trees = layer.GetFeatureCount()
        self.assertEqual(165, number_trees)
        data = None

        # voronoi
        self.assertFalse(exists('{}.shp'.format(self.voronoi3)))
