# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : February 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Tree Density Calculator plugin and treedensitycalculator python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os import remove
from os.path import join, dirname, realpath, exists
from osgeo import ogr
from localmaxfilter.interfaces.local_max_filter_cli import create_parser, run_local_max
from tests import ExtendedUnitTesting

data_folder = join(dirname(realpath(__file__)), "testdata")


class TestLMFCLI(ExtendedUnitTesting):

    image = join(data_folder, 'polyg_15_100cm.tif')
    mask = join(data_folder, 'polyg_15_mask.shp')
    output1 = join(data_folder, 'output1')
    out1_mask = '{0}_window_3_mask'.format(output1)
    out1_point = "{0}_window_3_point".format(output1)
    out1_voronoi = "{0}_window_3_voronoi".format(output1)
    output2 = join(data_folder, 'polyg_15_100cm')
    out2_mask = '{0}_window_3_mask'.format(output2)
    out2_point = "{0}_window_3_point".format(output2)
    out2_voronoi = "{0}_window_3_voronoi".format(output2)

    @classmethod
    def setUpClass(cls) -> None:
        parser = create_parser()
        # full option
        args = [cls.image, '3', '-m={}'.format(cls.mask), '-v', '-o={}'.format(cls.output1)]
        run_local_max(parser.parse_args(args))
        # minimal input
        args = [cls.image, '3']
        run_local_max(parser.parse_args(args))

    @classmethod
    def tearDownClass(cls) -> None:
        output_files = ['{}.shp'.format(cls.out1_mask), '{}.dbf'.format(cls.out1_mask),
                        '{}.prj'.format(cls.out1_mask), '{}.shx'.format(cls.out1_mask),
                        # '{}.shp'.format(cls.out1_point), '{}.dbf'.format(cls.out1_point),
                        '{}.prj'.format(cls.out1_point), '{}.shx'.format(cls.out1_point),
                        '{}.qpj'.format(cls.out1_point),
                        '{}.shp'.format(cls.out1_voronoi), '{}.dbf'.format(cls.out1_voronoi),
                        '{}.prj'.format(cls.out1_voronoi), '{}.shx'.format(cls.out1_voronoi),
                        '{}.qpj'.format(cls.out1_voronoi),
                        '{}.shp'.format(cls.out2_point), '{}.dbf'.format(cls.out2_point),
                        '{}.prj'.format(cls.out2_point), '{}.shx'.format(cls.out2_point),
                        '{}.qpj'.format(cls.out2_point),
                        '{}.aux.xml'.format(cls.image)]
        for file in output_files:
            if exists(file):
                remove(file)

    def test_mask(self):
        data = ogr.Open('{}.shp'.format(self.out1_mask))
        layer = data.GetLayer()
        tree_count = []
        for feature in layer:
            tree_count.append(int(feature.GetField("TreeCount")))
        self.assertEqual(81, tree_count[0])
        self.assertEqual(84, tree_count[1])
        data = None

    def test_point(self):
        data = ogr.Open('{}.shp'.format(self.out1_point))
        layer = data.GetLayer()
        number_trees = layer.GetFeatureCount()
        self.assertEqual(165, number_trees)
        data = None

    def test_voronoi(self):
        data = ogr.Open('{}.shp'.format(self.out1_voronoi))
        layer = data.GetLayer()
        number_polygons = layer.GetFeatureCount()
        self.assertEqual(165, number_polygons)
        data = None

    def test_system_exit(self):
        parser = create_parser()
        with self.assertRaises(SystemExit):
            run_local_max(parser.parse_args([]))
        with self.assertRaises(SystemExit):
            run_local_max(parser.parse_args([self.image]))

    def test_no_output(self):
        data = ogr.Open('{}.shp'.format(self.out2_point))
        layer = data.GetLayer()
        number_trees = layer.GetFeatureCount()
        self.assertEqual(394, number_trees)
        data = None
        self.assertFalse(exists('{}.shp'.format(self.out2_mask)))
        self.assertFalse(exists('{}.shp'.format(self.out2_voronoi)))
