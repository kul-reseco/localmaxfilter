# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : August 2018
| Copyright           : © 2018 - 2020 by Tinne Cahy (Geo Solutions) and Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the QGIS Tree Density Calculator plugin and treedensitycalculator python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
from os import remove
from os.path import join, dirname, realpath, splitext, exists
import numpy as np
from osgeo import ogr
from localmaxfilter.interfaces.imports import import_vector_as_image, import_image
from localmaxfilter.interfaces.exports import write_point_layer, write_mask_layer, write_voronoi_layer
from localmaxfilter.core.local_max_filter import LocalMaxFilter
from tests import ExtendedUnitTesting

data_folder = join(dirname(realpath(__file__)), "testdata")


class TestLMF(ExtendedUnitTesting):

    created_files = []

    def test_max(self):
        image = np.zeros((5, 5))
        for i in range(0, 5):
            image[1, i] = 1
        for i in range(0, 5):
            image[i, 1] = 1

        local_max_filter = LocalMaxFilter(3)
        result = local_max_filter.execute(image)
        self.assertListEqual(
            [
                {'RasterVal': 1.0, 'Pixel': {'x': 1, 'y': 1}},
                {'RasterVal': 1.0, 'Pixel': {'x': 2, 'y': 1}},
                {'RasterVal': 1.0, 'Pixel': {'x': 3, 'y': 1}},
                {'RasterVal': 1.0, 'Pixel': {'x': 1, 'y': 2}},
                {'RasterVal': 1.0, 'Pixel': {'x': 1, 'y': 3}}],
            result
        )

        local_max_filter = LocalMaxFilter(5)
        result = local_max_filter.execute(image)
        self.assertListEqual([], result)

    def test_no_projection(self):
        with self.assertRaises(Exception):
            import_image(join(data_folder, 'polyg_15_100cm_wgs.tif'))

    def test_polygon_without_id(self):
        image_path = join(data_folder, 'polyg_15_100cm.tif')
        mask_path = join(data_folder, 'polyg_15_mask_no_id.shp')
        image, image_srs, image_gt = import_image(image_path, mask_path, 5)
        poly_mask = import_vector_as_image(mask_path, image_gt, image.shape, image_srs, 5)

        # filter local max using certain window size
        local_max_filter = LocalMaxFilter(5)
        local_max_dict = local_max_filter.execute(image, area_of_interest=poly_mask, geo_transform=image_gt)

        # output file names
        output_base = join(data_folder, "polyg_15_100cm_no_id_output")
        output_mask_path = output_base + '_mask.shp'
        output_point_path = output_base + '_point.shp'
        self.created_files.append(output_mask_path)
        self.created_files.append(output_point_path)

        # output point layer
        write_point_layer(output_point_path, local_max_dict, image_gt, image_srs, mask_path=mask_path)

        # output mask layer
        write_mask_layer(output_mask_path, mask_path, output_point_path)

        # get mask layer
        driver = ogr.GetDriverByName('ESRI Shapefile')
        data = driver.Open(output_mask_path)
        layer = data.GetLayer()
        self.assertEqual(layer.GetFeature(0).GetField('nope'), 0)
        self.assertEqual(layer.GetFeature(1).GetField('nope'), 1)
        self.assertEqual(layer.GetFeature(0).GetField('Area_ha'), 0.1104)
        self.assertEqual(layer.GetFeature(1).GetField('Area_ha'), 0.1182)
        self.assertEqual(layer.GetFeature(0).GetField('TreeCount'), 41)
        self.assertEqual(layer.GetFeature(1).GetField('TreeCount'), 46)
        self.assertEqual(layer.GetFeature(0).GetField('TreeDens'), 371.40)
        self.assertEqual(layer.GetFeature(1).GetField('TreeDens'), 389.15)

    def test_exercise(self):

        results = {15: {5: 0, 3: 0}, 38: {5: 0, 3: 0}, 64: {5: 0, 3: 0}, 83: {5: 0, 3: 0}, 103: {5: 0, 3: 0}}
        for nr in results.keys():
            # input images to arrays
            image_path = join(data_folder, 'polyg_{}_100cm.tif'.format(nr))
            self.created_files.append('{}.aux.xml'.format(image_path))

            for window_length in [3, 5]:
                mask_path = join(data_folder, 'polyg_{}_mask.shp'.format(nr))
                # clipped image and metadata
                image, image_srs, image_gt = import_image(image_path, mask_path, window_length)
                # import area of interest as raster
                polyg_mask = import_vector_as_image(mask_path, image_gt, image.shape, image_srs, window_length)

                # filter local max using certain window size
                local_max_filter = LocalMaxFilter(window_length)
                local_max_dict = local_max_filter.execute(image, area_of_interest=polyg_mask, geo_transform=image_gt)

                # output file names
                output_base = join(data_folder, "polyg_{0}_100cm_max_{1}".format(nr, window_length))
                output_mask_path = output_base + '_mask.shp'
                output_point_path = output_base + '_point.shp'
                output_voronoi_path = output_base + '_voronoi.shp'
                self.created_files.append(output_mask_path)
                self.created_files.append(output_point_path)
                self.created_files.append(output_voronoi_path)

                # output point layer
                write_point_layer(output_point_path, local_max_dict, image_gt, image_srs, mask_path=mask_path)

                # output mask layer
                write_mask_layer(output_mask_path, mask_path, output_point_path)

                # get point layer
                driver = ogr.GetDriverByName('ESRI Shapefile')
                point_data = driver.Open(output_point_path)
                point_layer = point_data.GetLayer()

                # count local max
                results[nr][window_length] = point_layer.GetFeatureCount()

                # create voronoi
                write_voronoi_layer(output_voronoi_path, output_point_path, mask_path)

        self.assertEqual(87, results[15][5])  # / 0.3540577 ha => 361.52
        self.assertEqual(165, results[15][3])  # / 0.3540577 ha => 703.28
        self.assertEqual(647, results[38][5])  # / 2.9073039 ha => 222.54
        self.assertEqual(1210, results[38][3])  # / 2.9073039 ha => 416.19
        self.assertEqual(401, results[64][5])  # / 1.4447274 ha => 277.56
        self.assertEqual(731, results[64][3])  # / 1.4447274 ha => 505.98
        self.assertEqual(127, results[83][5])  # / 0.3341140 ha => 380.11
        self.assertEqual(192, results[83][3])  # / 0.3341140 ha => 574.65
        self.assertEqual(102, results[103][5])  # / 0.369157 ha => 276.31
        self.assertEqual(179, results[103][3])  # / 0.369157 ha => 484.89

    @classmethod
    def tearDownClass(cls) -> None:
        for created_file in cls.created_files:
            base, _ = splitext(created_file)
            all_files = [base + '.dbf', base + '.prj', base + '.shp', base + '.shx', base + '.qpj', base + '.xml']
            for file in all_files:
                if exists(file):
                    try:
                        remove(file)
                    except PermissionError:
                        pass
