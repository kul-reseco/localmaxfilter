User Guide
==========

The Tree Density Calculator can be used in several ways:

    1. As a plugin in QGIS
    2. From the QGIS processing toolbox
    3. As a commandline interface
    4. Adapting the code to fulfil very specific needs

For the last option, we refer the user to the `code repository <https://bitbucket.org/kul-reseco/localmaxfilter/src>`_
and the `API at the end of this document <reference.html>`_.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/localmaxfilter/issues?status=new&status=open>`_.

QGIS Plugin
-----------

.. image:: images/gui.PNG
   :width: 70%
   :align: center


#. Select the remote sensing image of the forest.

#. Determine the sliding window size.

#. Optionally, choose a polygon vector layer representing the area(s) of interest. Trees will only be counted inside
   these polygons. The polygons **must have an ID field**.

   .. note::

      For output, you will get an extra polygon layer with the following attributes: the polygon area in ha,
      the number of trees and the tree density (trees/ha).

#. Optionally, choose a snap distance. This distance should be maximum half of the window size.

#. Optionally, you can have an output layer with Voronoi polygons.

#. Optionally, choose a filename for the output layer(s).


Command Line Interface
----------------------

The main command is::

        >treedensity

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >treedensity -h

The **image** and **sliding window size** are required arguments. An example::

        >treedensity data\input\image.tif 3

By default, the output shapefiles are stored in the same folder as the image file,
with the extensions '_point.shp', '_mask.shp' and '_voronoi.shp'.
To select another file name base (no extension) or another location, use the argument :code:`-o` or :code:`--output`::

        >treedensity data\input\image.tif 3 -o data\other\new_name

To select a mask for counting the  tree density only in a given area, use the argument :code:`-m` or :code:`--mask`::

        >treedensity data\input\image.tif 3 -m data\input\mask.shp

To select a snap distance, use the argument :code:`-s` or :code:`--snap`.
This distance should be maximum half of the window size::

        >treedensity data\input\image.tif 7 -s 3

To create an output layer with voronoi polygons, use the argument :code:`-v` or :code:`--voronoi`::

        >treedensity data\input\image.tif 3 -v

