.. include:: ../../README.md

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/localmaxfilter/issues?status=new&status=open>`_.

.. image:: images/lumos_big.svg
   :width: 40 %

.. image:: images/logo.PNG
   :width: 20 %

.. toctree::
   :hidden:

   context
   installation
   userguide
   exercises
   reference

