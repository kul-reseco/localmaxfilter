Context
-------
*Tree density* is defined as the number of trees per hectare. Tree density and the evolution thereof can be used
to evaluate forest management or as parameter in estimations of volume of wood per hectare.
This forest inventory parameter usual is determined in the field by estimating the number of trees in selected
sample circles. This can be done by counting the trees (intensive) or by using specialised equipment like the
bitterlich.

Remote sensing offers an alternative for field measurements.
Obviously, these techniques will work best on high resolution imagery, provided by sensors like IKONOS or Quickbird.

The assumpiton in the **Tree Density Calculator** is that a treetop is the brightest part of a tree in remote
sensing imagery. The Tree Density Calculator will use a sliding window to move over the image.
For every position of the window, the Tree Density Calculator checks whether the central pixel is the brightest of the
window. If so, the pixel is marked as a *local maximum*.

.. figure:: images/sliding_window.PNG
   :width: 50%

   The Tree Density Calculator recoginzes the treetop as the local maximum in brightmess within the window of 5x5
   pixels.


.. figure:: images/local_maxima.PNG
   :width: 50%

   An example of the result of applying the Tree Density Calculator on a 50 cm resolution image.

We expect the best results for homogeneous coniferous forests. Conifers (especially spruce trees) have a very
distinct conical treetop, of which the top is indeed the brightest part.

In it's most basic form, the *Tree Density Calculator* has only one parameter: the sliding window size.
Depending on the resolution of the images and the size of the treetops, it is a process of trial and error to find
the correct window size.