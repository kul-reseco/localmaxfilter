Tree Density Calculator API
===========================

Source code: https://bitbucket.org/kul-reseco/localmaxfilter/src.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/localmaxfilter/issues?status=new&status=open>`_.

Core
----

.. automodule:: localmaxfilter.core.local_max_filter
    :members:
    :undoc-members:
    :show-inheritance:

Interfaces
----------

.. automodule:: localmaxfilter.interfaces
    :members:
    :undoc-members:
    :show-inheritance:



.. automodule:: localmaxfilter.interfaces.imports
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: localmaxfilter.interfaces.exports
    :members:
    :undoc-members:
    :show-inheritance:


.. automodule:: localmaxfilter.interfaces.local_max_filter_gui
    :members: LocalMaxFilterWidget
    :undoc-members:

CLI
---

.. argparse::
   :module: localmaxfilter.interfaces.local_max_filter_cli
   :func: create_parser
   :prog: treedensity

